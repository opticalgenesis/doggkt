plugins {
    id("com.android.application")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    id("androidx.navigation.safeargs")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")

}

android {
    compileSdkVersion(29)
    buildToolsVersion = "29.0.3"

    aaptOptions {
        noCompress(".tflite", ".txt")
    }

    defaultConfig {
        applicationId = "com.jfelt.dogg"
        minSdkVersion(21)
        targetSdkVersion(29)
        versionCode = 7
        versionName = "0.0.1-alpha04"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = org.gradle.api.JavaVersion.VERSION_1_8
        targetCompatibility = org.gradle.api.JavaVersion.VERSION_1_8
    }
}

dependencies {
    val navVersion = "2.3.0-alpha02"
    val camXVersion = "1.0.0-alpha06"
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.3.61")
    implementation("androidx.core:core-ktx:1.2.0")
    implementation("androidx.appcompat:appcompat:1.1.0")
    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation("androidx.navigation:navigation-fragment-ktx:$navVersion")
    implementation("androidx.navigation:navigation-ui-ktx:$navVersion")
    implementation("androidx.preference:preference:1.1.0")
    implementation("androidx.camera:camera-core:$camXVersion")
    implementation("androidx.camera:camera-camera2:$camXVersion")
    implementation("com.google.firebase:firebase-auth:19.2.0")
    implementation("com.google.android.gms:play-services-auth:17.0.0")
    implementation("com.google.firebase:firebase-crashlytics:17.0.0-beta01")
    implementation("com.google.firebase:firebase-ml-model-interpreter:22.0.1")
    implementation("com.google.firebase:firebase-storage-ktx:19.1.1")
    implementation("com.google.firebase:firebase-firestore-ktx:21.4.1")
    implementation("com.google.guava:listenablefuture:9999.0-empty-to-avoid-conflict-with-guava")
    implementation("com.google.android.material:material:1.1.0")
    implementation("com.github.bumptech.glide:glide:4.11.0")
    kapt("com.github.bumptech.glide:compiler:4.11.0")
    testImplementation("junit:junit:4.12")
    androidTestImplementation("androidx.test.ext:junit:1.1.1")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.2.0")

}