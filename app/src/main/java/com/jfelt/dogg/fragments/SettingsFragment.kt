package com.jfelt.dogg.fragments

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.jfelt.dogg.R

class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.prefs_main, rootKey)
    }
}