package com.jfelt.dogg.fragments

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.jfelt.dogg.R

private const val READ_STORAGE_REQUEST_CODE = 9001
private val STORAGE_PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)

class ImageSelectFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_image_select, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arePermissionsGranted()) {
            launchImagePicker()
        } else {
            requestPermissions(STORAGE_PERMISSIONS, READ_STORAGE_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            READ_STORAGE_REQUEST_CODE -> {
                if (arePermissionsGranted()) {
                    launchImagePicker()
                } else {
                    Toast.makeText(requireContext(), "Required permissions missing", Toast.LENGTH_LONG).show()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            9002 -> {
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        parseData(data.data!!)
                    }
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun parseData(u: Uri) {
        val action = ImageSelectFragmentDirections.actionImageSelectFragmentToDisplayProspectiveImageFragment(
            null,
            u.toString()
        )
        findNavController().navigate(action)
    }

    private fun launchImagePicker() {
        val imageIntent = Intent()
        imageIntent.type = "image/*"
        imageIntent.action = Intent.ACTION_GET_CONTENT

        startActivityForResult(imageIntent, 9002)
    }

    private fun arePermissionsGranted() = STORAGE_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
    }
}