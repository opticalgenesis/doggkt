package com.jfelt.dogg.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.jfelt.dogg.R

class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val cameraBtn: Button = view.findViewById(R.id.fragment_main_camera_btn)
        cameraBtn.setOnClickListener { findNavController().navigate(R.id.cameraFragment) }

        val chooseBtn: Button = view.findViewById(R.id.fragment_main_choose_btn)
        chooseBtn.setOnClickListener { findNavController().navigate(R.id.imageSelectFragment) }
    }
}