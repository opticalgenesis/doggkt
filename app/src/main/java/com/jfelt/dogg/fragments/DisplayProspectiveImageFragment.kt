package com.jfelt.dogg.fragments

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.jfelt.dogg.R

class DisplayProspectiveImageFragment : Fragment() {

    private lateinit var iv: ImageView
    private lateinit var confirm: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_img_display, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iv = view.findViewById(R.id.display_image_view)
        confirm = view.findViewById(R.id.btn_confirm)

        if (arguments?.getString("imgPath") != null) {
            loadImgFromPath(arguments?.getString("imgPath")!!)
        } else if (arguments?.getString("contentPath") != null) {
            loadImgFromContentUri(arguments?.getString("contentPath")!!)
        }
    }

    private fun loadImgFromPath(path: String) {
        Glide.with(this).load(path).into(iv)
        confirm.setOnClickListener {
            val a = DisplayProspectiveImageFragmentDirections.actionDisplayProspectiveImageFragmentToThinkingFragment(
                path, null
            )
            findNavController().navigate(a)
        }
    }

    private fun loadImgFromContentUri(path: String) {
        Glide.with(this).load(Uri.parse(path)).into(iv)
        confirm.setOnClickListener {
            val a = DisplayProspectiveImageFragmentDirections.actionDisplayProspectiveImageFragmentToThinkingFragment(
                null, path
            )
            findNavController().navigate(a)
        }
    }
}