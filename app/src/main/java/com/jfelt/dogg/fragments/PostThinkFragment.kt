package com.jfelt.dogg.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.jfelt.dogg.R
import java.io.File
import java.lang.Exception

class PostThinkFragment : Fragment() {

    private lateinit var auth: FirebaseAuth

    private val bIsFilePath by lazy { arguments!!.getString("imgPath") != null }
    private val storage by lazy { FirebaseStorage.getInstance() }
    private val database by lazy { FirebaseFirestore.getInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val dialog = AlertDialog.Builder(requireContext())
        dialog.apply {
            setTitle("Consent")
            setMessage("By uploading your image you consent to the conditions outlined in the privacy policy including a unique, traceable identifier being stored with your image to prevent abuse of the upload system. Please keep your submissions to dog pictures! Signing in with Google is required for this step to track any naughty images back to you.")
                .setPositiveButton("Confirm"){ dialogInterface, _ ->
                    dialogInterface.dismiss()
                    auth = FirebaseAuth.getInstance()
                    continueSubmission()
                }
                .setNegativeButton("Decline") { dialog, _ ->
                    dialog.cancel()
                    findNavController().navigate(R.id.mainFragment)
                }
        }.create().show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            6969 -> {
                val t = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    val acc = t.getResult(ApiException::class.java)
                    loginWithGoogle(acc!!)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun loginWithGoogle(acc: GoogleSignInAccount) {
        val creds = GoogleAuthProvider.getCredential(acc.idToken, null)
        auth.signInWithCredential(creds)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    Toast.makeText(requireContext(), "Successfully signed in!", Toast.LENGTH_LONG).show()
                    continueSubmission()
                } else {
                    Log.e("ML_DOGG", it.exception?.message!!)
                }
            }
    }

    private fun continueSubmission() {
        if (auth.currentUser == null) {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .apply {
                    requestEmail()
                    requestId()
                    requestIdToken(requireActivity().resources.getString(R.string.google_signin_key))
                }.build()

            val client = GoogleSignIn.getClient(requireActivity(), gso)
            startActivityForResult(client.signInIntent, 6969)
        } else {
            database.collection("banned_users").get()
                .addOnSuccessListener {
                    for (d in it) {
                        if (auth.currentUser!!.uid == d["user_id"]) {
                            // TODO add reason
                            Toast.makeText(requireContext(), "You have been banned :(", Toast.LENGTH_LONG).show()
                            return@addOnSuccessListener
                        } else {
                            if (bIsFilePath) {
                                postFile()
                            } else {
                                postContent()
                            }
                        }
                    }
                }
        }
    }

    private fun postFile() {
        Toast.makeText(requireContext(), "Posting from file", Toast.LENGTH_LONG).show()
        val file = Uri.fromFile(File(arguments?.getString("imgPath")!!))
        val ref = storage.reference.child("${arguments?.getString("breedName")}/${file.lastPathSegment}")
        val t = ref.putFile(file)
        val task = t.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let { throw it }
            }
            ref.downloadUrl
        } .addOnCompleteListener {
                Log.d("ML_DOGG", "Img uploaded to ${ref.path}")
                val submission = hashMapOf(
                    "breed" to arguments!!.getString("breedName"),
                    "bucket_url" to it.result.toString()
                )
                database.collection("submissions")
                    .document(auth.currentUser!!.uid).get()
                    .addOnSuccessListener { s ->
                        if (s.contains("submissions")) {
                            database.collection("submissions")
                                .document(auth.currentUser!!.uid)
                                .update("submissions", FieldValue.arrayUnion(submission))
                        } else {
                            database.collection("submissions")
                                .document(auth.currentUser!!.uid)
                                .set(mapOf("submissions" to listOf(submission)))
                        }
                    }
            }
            .addOnFailureListener {
                Log.wtf("ML_DOGG", "upload wtf??")
            }
    }

    private fun postContent() {}
}