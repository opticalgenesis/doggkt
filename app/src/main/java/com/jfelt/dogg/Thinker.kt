package com.jfelt.dogg

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.BatteryManager
import android.util.Log
import androidx.preference.PreferenceManager
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager
import com.google.firebase.ml.custom.*

class Thinker {
    companion object {
        val remoteModel by lazy {FirebaseCustomRemoteModel.Builder("nasnet").build()}
        val localModel by lazy {FirebaseCustomLocalModel.Builder()
            .setAssetFilePath("d_old.tflite")
            .build()}

        @JvmStatic
        fun downloadModel(ctx: Context) {
            Log.d("ML_DOGG", "dling")
            val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(ctx)
            val bShouldDownloadOnMobile = sharedPrefs.getBoolean("download_model_on_mobile", false)
            val bShouldDownloadOnAc = sharedPrefs.getBoolean("download_model_on_charge", false)

            val cm: ConnectivityManager = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val bIsMetered = cm.isActiveNetworkMetered

            // requires API24
            val batteryStatus: Intent? = ctx.registerReceiver(null,
                IntentFilter(Intent.ACTION_BATTERY_CHANGED)
            )
            val status = batteryStatus?.getIntExtra(BatteryManager.EXTRA_STATUS, -1) ?: -1
            val bIsCharging = status == BatteryManager.BATTERY_STATUS_CHARGING

            val bCanUseMobile = bShouldDownloadOnMobile == bIsMetered
            val bMeetsPwrReqmt = bShouldDownloadOnAc == bIsCharging

            val conditions = FirebaseModelDownloadConditions.Builder()
            if (!bCanUseMobile) conditions.requireWifi()
            val built = conditions.build()

            FirebaseModelManager.getInstance().download(remoteModel, built)
                .addOnCompleteListener {
                    Log.d("ML_DOGG", "Model downloaded successfully")
                }
                .addOnFailureListener {
                    Log.d("ML_DOGG", "Model failed to download ${it.message}")
                }
                .addOnSuccessListener {
                    Log.d("ML_DOGG", "Model downloaded successfully")
                }
        }

        @JvmStatic
        fun think(bmp: Bitmap, li: OnDoggoDetectedListener) {
            Log.d("DOGG", "thonk")
            val scBmp = Bitmap.createScaledBitmap(bmp, 224, 224, true)
            var interpreter: FirebaseModelInterpreter? = null
            FirebaseModelManager.getInstance().isModelDownloaded(remoteModel)
                .addOnSuccessListener { b ->
                    if (b) {
                        val opts = FirebaseModelInterpreterOptions.Builder(remoteModel).build()
                        interpreter = FirebaseModelInterpreter.getInstance(opts)
                        infer(scBmp, interpreter, li)
                    } else {
                        Log.d("ML_DOGG", "Using local model")
                        val opts = FirebaseModelInterpreterOptions.Builder(localModel).build()
                        interpreter = FirebaseModelInterpreter.getInstance(opts)
                        infer(scBmp, interpreter, li)
                    }
                }
        }

        private fun infer(bmp: Bitmap, interpreter: FirebaseModelInterpreter?,
                          li: OnDoggoDetectedListener): FloatArray? {
            var result: FloatArray? = null
            val iOpts = FirebaseModelInputOutputOptions.Builder()
                .setInputFormat(0, FirebaseModelDataType.FLOAT32, intArrayOf(1, 224, 224, 3))
                .setOutputFormat(0, FirebaseModelDataType.FLOAT32, intArrayOf(1, 120)).build()

            val bNum = 0
            val input = Array(1) { Array(224) {Array(224) {FloatArray(3)} } }
            for (x in 0..223) {
                for (y in 0..223) {
                    val px = bmp.getPixel(x, y)
                    input[bNum][x][y][0] = (Color.red(px) - 127) / 255.0f
                    input[bNum][x][y][1] = (Color.green(px) - 127) / 255.0f
                    input[bNum][x][y][2] = (Color.blue(px) - 127) / 255.0f
                }
            }
            val inputs = FirebaseModelInputs.Builder()
                .add(input).build()

            if (interpreter == null) Log.e("ML_DOGG", "wot")

            interpreter?.run(inputs, iOpts)
                ?.addOnSuccessListener { res ->
                    val out = res.getOutput<Array<FloatArray>>(0)
                    result = out[0]
                    Log.d("ML_DOGG", "doggo")
                    li.onDoggoDetected(result)
                }
                ?.addOnFailureListener { e ->
                    Log.e("ML_DOGG", ":(: ${e.message}")
                    e.printStackTrace()
                }
            return result
        }
    }

    interface OnDoggoDetectedListener {
        fun onDoggoDetected(array: FloatArray?)
    }
}